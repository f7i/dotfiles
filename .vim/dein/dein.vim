if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=$HOME/.vim/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state($HOME . '/.vim/dein/')
  call dein#begin($HOME . '/.vim/dein/')

  " Let dein manage dein
  " Required:
  call dein#add($HOME . '/.vim/dein/repos/github.com/Shougo/dein.vim')

  " TOMLの定義
  let s:toml_dir = $HOME . '/.vim/dein/'
  let s:toml     = s:toml_dir . '/dein.toml'
  let s:toml_lazy= s:toml_dir . '/lazy.toml'

  " キャッシュしておく
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:toml_lazy, {'lazy': 1})


  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

" Color Scheme
colorscheme molokai
set t_Co=256
syntax enable
