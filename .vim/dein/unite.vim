""" unite.vim

let g:unite_enable_start_insert=0
let g:unite_source_history_yank_enable=1
let g:unite_source_file_mru_limit = 100

" key mapping
nnoremap [unite] <nop>
nmap <Space>u [unite]

nnoremap <silent> [unite]b :<C-u>Unite buffer<CR>
nnoremap <silent> [unite]m :<C-u>Unite file_mru<CR>
nnoremap <silent> [unite]y :<C-u>Unite history/yank<CR>
nnoremap <silent> [unite]u :<C-u>Unite buffer file_mru<CR>

