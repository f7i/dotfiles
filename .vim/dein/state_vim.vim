if g:dein#_cache_version != 100 | throw 'Cache loading error' | endif
let [plugins, ftplugin] = dein#load_cache_raw(['/var/home/k-fuchi/.vimrc', '/home/k-fuchi/.vim/dein//dein.toml', '/home/k-fuchi/.vim/dein//lazy.toml'])
if empty(plugins) | throw 'Cache loading error' | endif
let g:dein#_plugins = plugins
let g:dein#_ftplugin = ftplugin
let g:dein#_base_path = '/home/k-fuchi/.vim/dein'
let g:dein#_runtime_path = '/home/k-fuchi/.vim/dein/.cache/.vimrc/.dein'
let g:dein#_cache_path = '/home/k-fuchi/.vim/dein/.cache/.vimrc'
let &runtimepath = '/var/home/k-fuchi/.vim,/usr/local/share/vim/vimfiles,/home/k-fuchi/.vim/dein/repos/github.com/Shougo/dein.vim,/home/k-fuchi/.vim/dein/.cache/.vimrc/.dein,/usr/local/share/vim/vim80,/home/k-fuchi/.vim/dein/.cache/.vimrc/.dein/after,/usr/local/share/vim/vimfiles/after,/var/home/k-fuchi/.vim/after'
source ~/.vim/dein/unite.vim
    let g:ale_sign_column_always = 1        " シンボルカラムを常に表示
    let g:ale_lint_on_save = 1              " 保存時にlint
    let g:ale_lint_on_text_changed = 1      " 入力時にlint
    let g:ale_lint_on_enter = 1             " ファイルオープン時にlint
    let g:ale_statusline_format = ['e:%d', 'w:%d', 'ok']
    let g:ale_echo_msg_error_str = 'E'
    let g:ale_echo_msg_warning_str = 'W'
    let g:ale_echo_msg_format = '%linter% [%severity%]: %s'
    let g:lightline = {   'colorscheme': 'one',   'active': {       'left': [           ['mode', 'paste'],           ['readonly', 'filename', 'modified', 'ale'],       ]   },   'component_function': {       'ale': 'ALEGetStatusLine'   }}
autocmd dein-events InsertCharPre * call dein#autoload#_on_event("InsertCharPre", ['neosnippet.vim'])
autocmd dein-events InsertEnter * call dein#autoload#_on_event("InsertEnter", ['neocomplete.vim'])
