set encoding=utf-8
scriptencoding utf-8

" 基本
set number          " 行番号表示
"set cursorline      " カーソル行のハイライト(重い)
"set showmatch       " 括弧対応確認
let loaded_matchparen = 0   " matchparen の無効化
set wildmenu        " ファイル名保管
set wildmode=full   " ファイル名保管

" ステータス行
set laststatus=2    " ステータス行を常に表示
set showmode        " モード表示
set showcmd         " コマンドを表示
set ruler           " カーソル位置表示

" インデントの設定
set expandtab       " Tabをspaceで入力
set tabstop=4       " タブ幅
set softtabstop=4   " カーソル動作の幅
set autoindent      " 改行時にインデントを保持
set smartindent     " 改行時のインデントを自動で増減
set shiftwidth=4    " smartindentでシフトする幅

" 検索の設定
"set incsearch       " インクリメンタルサーチ
set ignorecase      " 大文字小文字を区別しない
"set smartcase       " 大文字小文字を区別する
set hlsearch        " 検索結果のハイライト


" 文字コード関連
set fileencoding=utf-8                  " 保存時の文字コード
set fileencodings=utf-8,euc-jp,cp932    " 読み込み時の文字コード（左側優先）
set fileformats=unix,dos,mac            " 改行コードの自動判別順
set ambiwidth=double                    " 一部の文字が崩れる問題の回避

" カラー
if !has('gui_running')
    set t_Co=256
endif
if $TERM == 'screen'
    set t_Co=256
endif

"colorscheme desert

" Load to dein(plugin manager) & Plugins
runtime dein/dein.vim

