#!/bin/sh
# dotfiles installer for Unixs

echo "Starting dotfiles install..."
if [ $# -eq 1 ]; then
  case $1 in
    /*) INSTALL_DIR=$1;;
    *) INSTALL_DIR=$PWD/$1;;
  esac
else
  INSTALL_DIR=$PWD/dotfiles
fi

# Convert the installation directory to absolute path
echo "Install to \"$INSTALL_DIR\"..."
if [ -e "$INSTALL_DIR" ]; then
  echo "\"$INSTALL_DIR\" already exists!"
fi

echo ""

# check git command
echo "[Check git command]"
type git || {
  echo 'Please install git or update your path to include the git executable!'
  exit 1
}
echo "Done."
echo ""

# make plugin dir and fetch dein
echo "[Fetch dotfiles]"
if ! [ -e "$INSTALL_DIR" ]; then
  echo "Begin fetching dotfiles..."
  mkdir -p "$INSTALL_DIR"
  git clone https://bitbucket.org/f7i/dotfiles.git $INSTALL_DIR
  echo "Done."
  echo ""
else
  echo "$INSTALL_DIR is not exists."
  exit 1
fi

# created symlinks
echo "[Create symlink]"
cd $INSTALL_DIR
for fname in .??*
do
    [ "$fname" == ".git" ] && continue

    ln -snfv $INSTALL_DIR/$fname ~/
done
echo "Done."
echo ""

# Setup vim
echo "[Setup Vim]"
VIM_DEIN_DIR="${INSTALL_DIR}/.vim/dein/repos/github.com/Shougo/dein.vim"
VIM_DICT_DIR="${INSTALL_DIR}/.vim/dict"
VIM_DEIN_REPO="https://github.com/Shougo/dein.vim"
PHPMAN_ARCHIVE_PATH="http://jp2.php.net/distributions/manual/php_manual_ja.tar.gz"

# install dein
echo "Begin fetching dein..."
mkdir -p "$VIM_DEIN_DIR"
git clone "$VIM_DEIN_REPO" "$VIM_DEIN_DIR"
echo "Done."
echo ""

# install php manuals
echo "download & restore the php manual..."
curl -fsSL $PHPMAN_ARCHIVE_PATH | tar -C $VIM_DICT_DIR -zxf -
echo "Done."
echo ""

echo "Complete install dotfiles!"
