f7i's dotfiles
==============

```
#!sh
    echo "proxy=proxy.example.com" >> ~/.curlrc
    bash -c "$(curl -fsSL https://bitbucket.org/f7i/dotfiles/raw/master/install.sh)"
```
